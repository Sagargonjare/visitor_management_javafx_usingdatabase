package showoptions;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import stageMaster.StageMaster;

public class ShowOptions  {
	
	public void show() {
	try{
		Parent actorGroup =FXMLLoader.load(getClass().getResource(getClass().getSimpleName()+".fxml"));
		StageMaster.getStage().setScene(new Scene(actorGroup));
		StageMaster.getStage().show();
	}catch(IOException e) {
		e.printStackTrace();
	}

	}
}
