package showoptions;

import createPass.Createpass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ShowOptionsController {
	@FXML
	private Button checkin;
	@FXML
	private Button checkout;
	
	public void checkin(ActionEvent event) {
		new Createpass().show();
	}
	public void checkout(ActionEvent event) {
		
	}
}
