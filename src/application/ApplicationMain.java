package application;

import dbOperations.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import showoptions.ShowOptions;
import stageMaster.StageMaster;

public class ApplicationMain extends Application {
	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
		
	}
	
	
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new ShowOptions().show();
	}
}
